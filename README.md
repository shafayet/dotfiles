<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#sec-1">1. Dotfiles</a></li>
<li><a href="#sec-2">2. Usage</a></li>
</ul>
</div>
</div>

# Dotfiles<a id="sec-1" name="sec-1"></a>

Bootstrapping my dotfiles

# Usage<a id="sec-2" name="sec-2"></a>

Run the following command to bootstrap all settings.

    git clone https://github.com/shafayetkhan/dotfiles.git ~/.dotfiles
    cd ~/.dotfiles
    script/bootstrap